> Plantilla Nuxt para Dinamo

[TOC]

# Build Setup

``` bash
#instala lo que este configurado en el repositorio y branch seleccionado.
yarn

#equivalente a yarn run start inicia el proyecto y actualiza el navegador
yarn dev

#Equivalente a yarn run build genera los archivos necesarios para publicar en el FTP
yarn generate
```

# Deploy
[TODO]

# Herramientas usadas

- Buefy
- Bulma
- Vue
- Nuxt
- Lodash

# Linter
Estamos usando un verificiador de estilo y errores automático para ayudarnos tanto a mantener un código consistente como fácil de mantener y con la menor cantidad de errores posible.

El estilo de código que usamos y por consiguiente las reglas que se siguen son una mezcla de [airbnb/base](https://github.com/airbnb/javascript#table-of-contents) y [vue/recomended](https://vuejs.org/v2/style-guide/). Airbnb funciona como base y vue como complemento. En los enlaces están las referencias a las reglas que se usan con ejemplo de uso y razonamiento detrás de la regla. En el caso de las reglas propietarias de vue [esta es la referencia completa](https://github.com/vuejs/eslint-plugin-vue#bulb-rules) ya que sigue en desarrollo.

Es muy recomendado instalar los complementos de eslint para el editor ya que estos te explican en que consiste un error y así corregirlo conforme se escribe.

#### Atom

- [linter-eslint](https://atom.io/packages/linter-eslint)
- [linter](https://atom.io/packages/linter)
- [linter-ui-default](https://atom.io/packages/linter-ui-default)
- [language-vue](https://atom.io/packages/language-vue)

Configuración recomendada para ver los errores en la sección `<template>`:

``` cson
  linter: {}
  "linter-eslint":
    fixOnSave: true
    lintHtmlFiles: true
    scopes: [
      "source.js"
      "source.jsx"
      "source.js.jsx"
      "source.babel"
      "source.js-semantic"
      "source.vue"
      "text.html.vue"
    ]
```

# VScode

[TODO]

Alrededor de la mitad de los errores que se marcan se pueden arreglar de forma automática. 

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).
